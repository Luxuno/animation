import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(LogoApp());
}

class LogoApp extends StatefulWidget{
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller=
        AnimationController(duration:  const Duration(milliseconds: 2000), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
    ..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
      controller.reverse();
      } else if(status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });

    controller.forward();
  }

  //@override
  /*Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        height: animation.value,
        width: animation.value,
        child: FlutterLogo(),
      ),
    );
  }*/
  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);
  static final _rotation = Tween<double>(begin:0, end: 80);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);
  
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Container(
      color: Colors.white,
      child: Transform.rotate( //Just rotates
        angle: _rotation.evaluate(animation),
        child: Opacity(
          opacity: _opacityTween.evaluate(animation),
          child: Container(
            child: Image.asset('images/Batman-logo.png'),
          ),
        ),
      ),
    );
  }
}